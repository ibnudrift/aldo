<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];
    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;

?>

	<!-- start section 4 home -->
	<section class="block_section_home_4 mt-4">
		<div class="prelatif container">
			<div class="insides">

			<div class="row default">
				<div class="col-md-6 separete_right">
					<div class="call_me">
						<div class="row">
							<div class="col-md-5">
								<div class="pict icons_asistant text-center pt-1">
									<img src="<?php echo $this->assetBaseurl2 ?>section4_home_banner_call_me.png" alt="" class="img img-fluid">
								</div>
							</div>
							<div class="col-md-7">
								<div class="descs">
									<h3>Hubungi Asisten Kami :</h3>
									<?php 
									$l_wa1 =  '62' . substr(str_replace(' ', '', $this->setting['contact_wa1']), 1);
									$l_wa2 =  '62' . substr(str_replace(' ', '', $this->setting['contact_wa2']), 1);
									?>
									<span><a href="https://wa.me/<?php echo $l_wa1 ?>"><i class="fa fa-whatsapp"></i>&nbsp;&nbsp;<?php echo $this->setting['contact_wa1'] ?></a></span>
									<span style="display: block;"><a href="https://wa.me/<?php echo $l_wa2 ?>"><i class="fa fa-whatsapp"></i>&nbsp;&nbsp;<?php echo $this->setting['contact_wa2'] ?></a></span>
									<span style="display: block;"><a href="mailto:<?php echo $this->setting['contact_email'] ?>"><i class="fa fa-envelope"></i>&nbsp;<?php echo $this->setting['contact_email'] ?></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- start Block -->
				<div class="col-md-6">
					<div class="py-4"></div>
					<?php
					  $list_data_sosmed = [
					                        [
					                          'pict-logo'=>'section4_home_social_media1.png',
					                          'pict-logo2'=>'section4_home_social_media1_on.png',
					                          'link' => $this->setting['url_instagram'],
					                        ],
					                        [
					                          'pict-logo'=>'section4_home_social_media2.png',
					                          'pict-logo2'=>'section4_home_social_media2_on.png',
					                          'link' => $this->setting['url_youtube'],
					                        ],
					                        [
					                          'pict-logo'=>'section4_home_social_media3.png',
					                          'pict-logo2'=>'section4_home_social_media3_on.png',
					                          'link' => $this->setting['url_facebook'],
					                        ],
					                        [
					                          'pict-logo'=>'section4_home_social_media4.png',
					                          'pict-logo2'=>'section4_home_social_media4_on.png',
					                          'link' => $this->setting['url_tokopedia'],
					                        ],
					                        [
					                          'pict-logo'=>'section4_home_social_media5.png',
					                          'pict-logo2'=>'section4_home_social_media5_on.png',
					                          'link' => $this->setting['url_shopee'],
					                        ],
					                        
					                      ]; 
					?>
					<div class="social_media boc_bottom text-center">
						<div class="row justify-content-center">
							<?php foreach ($list_data_sosmed as $key => $value): ?>
							<div class="col-md-2 lists_img_socmed">
								<a target="_blank" href="<?php echo $value['link'] ?>"><img src="<?php echo $this->assetBaseurl2 . $value['pict-logo'] ?>" data-image-on="<?php echo $this->assetBaseurl2 . $value['pict-logo2'] ?>?rand=<?php echo mt_rand(1, 1000) ?>" data-image-of="<?php echo $this->assetBaseurl2 . $value['pict-logo'] ?>" alt="icons footer - <?php echo Yii::app()->name; ?>" class="img img-fluid"></a>
							</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</div>

			</div>
		</div>
	</section>
	<!-- end section 4 Home  -->

	<script type="text/javascript">
		$('.lists_img_socmed a img').mouseenter(function(){
			var data_img_active = $(this).attr('data-image-on');
			$(this).attr('src', data_img_active);
		});

		$('.lists_img_socmed a img').mouseleave(function(){
			var data_img_off = $(this).attr('data-image-of');
			$(this).attr('src', data_img_off);
		});
		
	</script>