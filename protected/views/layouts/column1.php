<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>
<div class="clear"></div>
<div class="middles_wrapper_cont tops_home">
    <!-- Start fcs -->
    <div class="fcs-wrapper outers_fcs_wrapper prelatife container">
        <div id="myCarousel_home" class="carousel fade" data-ride="carousel">
            <?php
            $criteria=new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->addCondition('active = 1');
            $criteria->params[':language_id'] = $this->languageID;
            $criteria->group = 't.id';
            $criteria->order = 't.id ASC';
            $slide = Slide::model()->with(array('description'))->findAll($criteria);
            ?>
            <ol class="carousel-indicators">
                <?php foreach ($slide as $key => $value): ?>
                    <li data-target="#myCarousel_home" data-slide-to="<?php echo $key; ?>" <?php if($key == 0){ ?>class="active"><?php } ?></li>
                <?php endforeach; ?>
            </ol>
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="item <?php if($key == 0){ ?>active<?php } ?>"> 
                    <a style="cursor: default;" href="#<?php // echo CHtml::normalizeUrl(array('/home/promoDetail', 'id'=> $value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1314,522, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive full_img"></a>
                </div>
                <?php endforeach; ?>
            </div>

            <!-- Controls -->
              <a class="left carousel-control" href="#myCarousel_home" role="button" data-slide="prev">
                <img src="<?php echo $this->assetBaseurl2 . 'navs_left_fcs.png'; ?>" alt="" class="img-responsive">
              </a>
              <a class="right carousel-control" href="#myCarousel_home" role="button" data-slide="next">
                <img src="<?php echo $this->assetBaseurl2 . 'navs_right_fcs.png'; ?>" alt="" class="img-responsive">
              </a>
        </div>
        <div class="backs_shadow_bottom_fn"></div>
        <div class="clear"></div>
    </div>
    <!-- End fcs -->
   
   <div class="clear"></div>
</div>
<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer2', array()); ?>
<?php $this->endContent(); ?>