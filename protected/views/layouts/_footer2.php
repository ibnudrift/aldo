<?php echo $this->renderPartial('//layouts/_top_foots', array()); ?>

<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

?>

	<!-- start Footer -->
	<footer class="foot">
		<div class="tops_footer back_orange">
			<div class="prelatif container">
				<div class="items">
					<div class="row">
						<div class="col-md-4">
							<div class="pict py-5 logo_foot">
								<img src="<?php echo $this->assetBaseurl2 ?>footer_logo.png" alt="" class="img img-fluid">
							</div>
						</div>
						<div class="col-md-4">
							<div class="category text-left">
								<h4>KATEGORI PILIHAN</h4>
								<div class="py-1"></div>
								<?php
								$criteria = new CDbCriteria;
								$criteria->with = array('description');
								$criteria->addCondition('t.type = :type');
								$criteria->params[':type'] = 'category';
								$criteria->addCondition('description.language_id = :language_id');
								$criteria->params[':language_id'] = $this->languageID;
								$criteria->addCondition('t.parent_id = "0"');
								$criteria->order = 'sort ASC';
								$dataCategory = PrdCategory::model()->findAll($criteria);

								$res_foot = array_chunk( $dataCategory, 2);
								?>
								<div class="item_category">
									<div class="row">
										<?php foreach ($res_foot as $ken => $van): ?>
										<div class="col-md-6">
											<ul>
												<?php foreach ($van as $key => $value): ?>
												<li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a></li>
												<?php endforeach ?>
											</ul>
										</div>
										<?php endforeach ?>

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="login_register text-right">
								<h4>MEMBER ALDO</h4>
								<div class="py-1"></div>
								<?php if ($login_member == null): ?>
								<span><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">Masuk</a></span>
								<?php else: ?>
								<span><a href="<?php echo CHtml::normalizeUrl(array('/member/logout')); ?>">Keluar</a></span>
								<?php endif ?>
							</div>
						</div>
				 	</div>
				</div>
				
			</div>
		</div>
		<div class="bottom_footer bg-dark">
			<div class="t-copyrights text-center py-3">
				<p class="m-0">Copyright &copy; ALDO TOOLS <?php echo date("Y"); ?> - Website design by <a href="http://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design.</a>.</p>
			</div>
		</div>
	</footer>
	<!-- end Footer -->