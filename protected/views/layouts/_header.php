<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;

$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('parent_id = 0');
$criteria->addCondition('type = "category"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'sort ASC';
$dataCategory = PrdCategory::model()->findAll($criteria);
?>

<div class="outers_back_headers">
  <header class="head">
    <div class="visible-lg visible-md">
      <div class="tops">
        <div class="prelatife container">
          <div class="row">
            <div class="col-md-5">
              <div class="clear height-5"></div>
              <div class="lgo_web_header d-inline">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                  <img src="<?php echo $this->assetBaseurl2 ?>logo-heads.png" alt="" class="img-responsive">
                </a>
              </div>
              <!-- <div class="taglineshdr_lefts d-inline padding-top-20">
                <img src="<?php echo $this->assetBaseurl ?>tx_tools_hardwar_header.png" alt="" class="img-responsive">
              </div> -->
            </div>
            <div class="col-md-7">
              <div class="rights_header prelatife">
                  <div class="d-inline b_search">
                    <form action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>" method="get">
                      <div class="form-group prelatife">
                        <input type="text" class="form-control" name="q" value="<?php echo $_GET['q'] ?>" placeholder="Cari">
                        <button type="submit" class="btn btn-link">
                          <i class="fa fa-search"></i>
                        </button>
                      </div>
                    </form>
                    <div class="clear"></div>
                  </div>

                  <div class="d-inline boxs_account_loginHeader">
                    <?php if ($login_member == null): ?>
                    <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                      <img src="<?php echo $this->assetBaseurl2 ?>icons-human.png" alt="" class="img-responsive d-inline"> &nbsp;
                      <span>Daftar/ Masuk</span>
                    </a>
                    <?php else: ?>
                    <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                      <i class="fa fa-bg_account"></i> &nbsp;
                      <span>Akun Saya</span>
                    </a>
                    <?php endif ?>
                    <div class="clear"></div>
                  </div>
                  <div class="d-inline frights back-grey boxs_bCart">
                    <a href="<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>">
                      <img src="<?php echo $this->assetBaseurl2 ?>icons-cart.png" alt="" class="img-responsive">
                    </a>
                    <div class="clear"></div>
                  </div>

                  <div class="clear"></div>
              </div>
            </div>
          <div class="clear"></div>
        </div>
        </div>
        <div class="clear"></div>
      </div>

    <div class="bottoms">
      <div class="prelatife">
        <div class="top-menu">
          <ul class="list-inline">
            <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">KATEGORI</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>">PROMO & BERITA</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>">DISTRIBUTOR</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">TENTANG KAMI</a></li>
            <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/blog/index')); ?>">TIPS & SARAN</a></li> -->
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">HUBUNGI KAMI</a></li>
          </ul>
          <div class="clear clearfix"></div>
        </div>
      </div>
      <div class="clear"></div>
    </div>

    </div>
    <!-- end bottom -->

      <div class="visible-sm visible-xs">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl2 ?>logo-heads.png" alt="Tools & Hardware" class="img-responsive">
              </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">KATEGORI</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>">PROMO & BERITA</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>">DISTRIBUTOR</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">TENTANG KAMI</a></li>
                <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/blog/index')); ?>">TIPS & SARAN</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">HUBUNGI KAMI</a></li>
              </ul>
              <div class="clear height-5"></div>
              <div class="bloc_aldoHeader">
                  <?php if ($login_member == null): ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Login</span>
                  </a>
                  <?php else: ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Akun Saya</span>
                  </a>
                  <?php endif ?>
                  <div class="boxs_bCart">
                    <a href="<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>">
                      <i class="fa fa-shopping-cart"></i> &nbsp;<span>My Cart</span>
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
              </div>

            </div>
          </div>
        </nav>

        <div class="tops_n_boxsearch py-2 back-white">
          <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">
          <div class="form-group mx-sm-3 mb-2">
            <label for="inputPassword2" class="sr-only">Password</label>
            <input type="text" class="form-control" name="q" id="inputPassword2" placeholder="Cari">
          </div>
          <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
        </form>
        </div>
        <div class="clear"></div>
      </div>

    <div class="clear"></div>
  </header>
</div>

<style type="text/css">
  .tops_n_boxsearch{
    position: relative;
  }
  .tops_n_boxsearch.py-2{
    padding: 10px 15px;
    border-bottom: 1px solid #ddd;
  }
  .tops_n_boxsearch form{
    margin-bottom: 0; margin: 0px;
    position: relative;
  }
  .tops_n_boxsearch form .form-group{
    margin-bottom: 0;
  }
  .tops_n_boxsearch input{
    border-radius: 0px;
  }
  .tops_n_boxsearch button{
    background: none; background-color: transparent;
    box-shadow: none;
    outline: none;
    border: 0px;

    position: absolute;
    top: 0px;
    right: 0px;
    color: #000;
  }
  .tops_n_boxsearch button i.fa{
    font-size: 15px;
  }
</style>

<script type="text/javascript">
  $(function(){
    $('#myAffix').affix({
      offset: {
        top: 300
      }
    })
  })
</script>

<section id="myAffix" class="header-affixs affix-top">
  <div class="clear height-15"></div>
  <div class="prelative container">
    <div class="row">
      <div class="col-md-4 col-sm-4">
        <div class="lgo_web_headrs_wb">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl2 ?>logo-heads.png" alt="" class="img-responsive">
          </a>
        </div>
      </div>
      <div class="col-md-8 col-sm-8">
        <div class="height-10"></div>
        <div class="text-right"> 
          <div class="menu-taffix">
            <ul class="list-inline d-inline">
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">KATEGORI</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>">PROMO & BERITA</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>">DISTRIBUTOR</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">TENTANG KAMI</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">HUBUNGI KAMI</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>