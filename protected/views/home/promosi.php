<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page sub_toppage_title">
    <div class="prelatife container">
      <h2 class="title_pg">PROMO & BERITA</h2>
    </div>
  </div>

  <div class="middle inside_content back-white">
    <div class="prelatife container">
      <div class="lists_data_promotions">
        <div class="row default_detail_p">
          <?php for ($i=1; $i < 4; $i++) { ?>
          <div class="col-md-4 col-sm-4">
            <div class="items">
              <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(388,388, '/images/static/'. $this->setting['promo_poster_'. $i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $this->default_alt; ?>" class="img-responsive">
            </div>
          </div>
          <?php } ?>
          
        </div>
      </div>
      <!-- end list promotions -->

      <div class="py-2"></div>
      <!-- <div class="lines-grey"></div>
      <div class="py-5"></div> -->
      <div class="py-3"></div>

      <div class="content-text text-left">
      <?php if ($dataBlog->getTotalItemCount() > 0): ?>
      <?php $data = $dataBlog->getData(); ?>
      <div class="outers_listing_news defaults_t ls_new_line_blog">
            <div class="row default">
              <?php foreach ($data as $key => $value): ?>
              <div class="col-md-12">
                <div class="items">
                    <div class="desc">
                        <div class="titles"><?php echo ucwords($value->description->title) ?></div>
                        <span class="dates">
                          <i class="fa fa-calendar"></i>&nbsp; <?php echo date("d F Y", strtotime($value->date_input)) ?>
                        </span>
                        <div class="py-1"></div>
                        <div class="descs">
                          <?php echo $value->description->content ?>
                        </div>
                    </div>
                </div>
              </div>
              <?php endforeach ?>
            </div>
            <div class="clear"></div>
        </div>
      <?php else: ?>
        <h3>No data blog</h3>
      <?php endif ?>
        <!-- end listing news -->
        <div class="clear height-10"></div>
      <div class="clear"></div>
    </div>
    <!-- end content berita artikel -->
    <?php /*
    <div class="text-center bgs_paginations">
      <?php $this->widget('CLinkPager', array(
        'pages' => $dataBlog->getPagination(),
        'header' => '',
        'htmlOptions' => array('class' => 'pagination'),
      )) ?>
    </div>
    */ ?>
      <!-- end blog -->

      <div class="py-3"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
