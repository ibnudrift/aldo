	<!-- start section slider 1 Home  -->
	<sectiion class="block_section_home_1">
		<div class="prelatif container">
			<div class="insides">
				<div class=""></div>
			</div>
		</div>
	</sectiion>
	<!-- end section 1 slider Home  -->

	<!-- start section 2 Home  -->
	<section class="block_section_home_2">
		<div class="prelatif container">
			<div class="insides">
				<div class="clear height-40"></div>
				<div class="category">
					<div class="items">
						<div class="title">
							<h2 class="mb-0">KATEGORI PILIHAN</h2>
						</div>
						<div class="clear height-10"></div>
						<div class="line_sparator"></div>
						<div class="clear height-25"></div>
						<!-- start Block -->
						<div class="content_category block_data_thum_products">
							<ul class="list-inline m-0">
								<?php foreach ($dataCategory as $key => $value): ?>
								<li class="list-inline-item">
									<div class="items">
										<div class="pic">
											<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(123,123, '/images/category/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->description->name; ?>" class="img img-fluid d-block mx-auto"></a>
										</div>
										<div class="info py-2 text-center">
											<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><span><?php echo $value->description->name ?></span></a>
										</div>
									</div>
								</li>
								<?php endforeach; ?>
							</ul>
						</div>
						<!-- end Block -->

						<div class="clear height-30"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end section 2 Home  -->

	<?php echo $this->renderPartial('//layouts/_block_orange', array()); ?>
	