<div class="clear"></div>
<div class="subpage defaults_static">
  <div class="top_title_page sub_toppage_title">
    <div class="prelatife container">
      <h2 class="title_pg mb-0">DISTRIBUTOR</h2>
    </div>
  </div>

  <div class="middle inside_content back-white">
    <div class="prelatife container">
      <div class="insides content-text text-center pLocationstore middles_pgStatic">

          <div class="boxeds_image_cen">
            <img src="<?php echo Yii::app()->baseUrl . '/asset/images/new/map-distributors.jpg'; ?>" alt="" class="img-responsive">
          </div>
          <div class="py-4"></div>
          <div class="py-1"></div>

          <h3>Kota Distributor ALDO Tool</h3>
          <div class="clear height-20"></div>
          <form action="" method="get" class="form-inline box_filter_location">
            <div class="row">
              <div class="form-group">
              <select name="kota" id="select-kota" class="form-control search_autocomplete" style="width: 625px; height: 60px;">
                <option value="">Nama Kota</option>
                <?php foreach ($listKota as $key => $value): ?>
                  <option value="<?php echo $value->kota ?>"><?php echo $value->kota ?></option>
                <?php endforeach ?>
              </select>
              <script type="text/javascript">
                $('#select-kota').val('<?php echo $_GET['kota'] ?>');
              </script>
              </div>
              <!-- <button type="submit" class="btn btn-primary">search</button> -->
            </div>
          </form>
        
        <div class="clear height-50"></div>
        <?php if (count($dataAddress) > 0): ?>
        <div class="block_top_lines_loc">
          <div class="lines-grey"></div>
          <span class="b_title">Daftar Distributor di</span>
        </div>
        <div class="clear height-20"></div>

        <div class="list_locaion_defaults_d">
          <?php foreach ($dataAddress as $key => $value): ?>
                    <div class="items">
                      <div class="titles"><?php echo $key ?></div>
                      <div class="clear height-20"></div>
          <?php
          $count_loc = count($value);
          $val = array_chunk($value, 3);
          ?>
                    <?php foreach ($val as $data_chunk): ?>
                    <div class="row">
                      <?php if ($count_loc == 2): ?>
                      <div class="col-md-2"></div>
                      <div class="col-md-8">
                      <?php endif; ?>
                      <?php foreach ($data_chunk as $k => $v): ?>
                        <?php if ($count_loc == 1): ?>
                        <div class="col-md-12 col-sm-12">
                        <?php elseif($count_loc == 2): ?>
                        <div class="col-md-6 col-sm-6">
                        <?php else: ?>
                        <div class="col-md-4 col-sm-4">
                        <?php endif ?>
                          <div class="item">
                            <p><b><?php echo $v->nama ?></b> <br>
                              <?php echo $v->address_1 ?><br />
                              <?php if ($v->address_2 != ''): ?>
                                <?php echo nl2br($v->address_2) ?><br />
                              <?php endif ?>
                            <?php if ($v->telp != ''): ?>
                            P. <?php echo $v->telp ?><br />
                            <?php endif ?>
                            <?php if ($v->fax != ''): ?>
                            F. <?php echo $v->fax ?> <br>
                            <?php endif ?>
                            <?php if ($v->email != ''): ?>
                            E. <?php echo $v->email ?>
                            <?php endif ?>
                            </p>
                            <div class="clear"></div>
                          </div>
                        </div>


                      <?php endforeach ?>
                        <?php if ($count_loc == 2): ?>
                        </div>
                        <div class="col-md-2"></div>
                        <?php endif ?>
                    </div>
                    <?php endforeach ?>
                      <div class="clear"></div>
                    </div>
          <?php endforeach ?>

                    <div class="clear"></div>
                  </div>
                  <!-- end list download item -->

                    <div class="clear height-50"></div>

                    <div class="clear height-25"></div>
                    <div class="clear"></div>
                  </div>
                  <!-- end insides -->
                </div>
              </div>
              <!-- End sub kategori -->
      <!-- end data location store -->
        <?php endif ?>

    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
      $('.search_autocomplete').select2();
  });
</script>