<div class="blocks_subpage_banner about mih393" style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['about_hero_image'] ?>')">
  <div class="prelatife blocks_text">
    <h3 class="sub_title_p"><?php echo $this->setting['about_hero_title'] ?></h3>
    <div class="clear"></div>
    <div class="lines_whites_md tengah"></div>
    <div class="clear height-20"></div>
    <p><?php echo nl2br($this->setting['about_hero_subtitle']) ?></p>
    <div class="clear"></div>
  </div>
</div>

<div class="clear"></div>
<div class="subpage static_about">

  <section class="blocks_middle_about1 picts_about_1">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="clear height-50"></div>
      <div class="content-text pl60">
        <div class="row">
          <div class="col-md-6">
            <h5><?php echo $this->setting['about_title'] ?></h5>
            <?php echo $this->setting['about_content'] ?>
            
          </div>
          <div class="col-md-6">
          </div>
        </div>
        
        <div class="clear height-35"></div>
      </div>

      <div class="clear"></div>
    </div>
    <div class="visible-xs visible-sm">
      <div class="picture_ab1"><img src="<?php echo $this->assetBaseurl ?>pict-middle-about_1.jpg" alt="" class="img-responsive"></div>
    </div>
  </section>

  <section class="default_sc blocks_bottom_about">
    <div class="prelatife container">
        <div class="insides content-text">
          <h4 class="sub_title">Our Commitment</h4>
          <div class="maw1022 tengah">
            <?php echo $this->setting['about_cmmitment'] ?>
          </div>
          <div class="clear height-35"></div>
          <div class="row">
            <div class="col-md-6 border_rg">
              <h2>visi</h2>
              <?php echo $this->setting['about_visi'] ?>
            </div>
            <div class="col-md-6">
              <h2>misi</h2>
              <?php echo $this->setting['about_misi'] ?>
            </div>
            <div class="col-md-6"></div>
          </div>
          <div class="clear"></div>
      </div>
    </div>
  </section>

  <div class="clear"></div>
</div>