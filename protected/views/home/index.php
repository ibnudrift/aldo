<div class="clear height-50"></div>
<section class="default_sc blocks_hom1 back-white">
	<div class="prelatife container">
		<div class="lists_banner_home_dt new_picture">
			<div class="row">
			<?php for ($i=1; $i < 5; $i++) { ?>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="items prelatife">
						<div class="pict">
							<a href="<?php echo $this->setting['home_banner_url_'.$i] ?>">
								<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,176, '/images/static/'.$this->setting['home_banner_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
		<div class="clear height-15"></div>

		<div class="banner_full_pic middle_thome">
			<div class="row">
			<?php for ($i=5; $i < 7; $i++) { ?>
				<div class="col-md-6">
					<a href="<?php echo $this->setting['home_banner_url_'.$i] ?>">
						<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(654,202, '/images/static/'.$this->setting['home_banner_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
					</a>
				</div>
			<?php } ?>
			</div>
		</div>

		<div class="clear height-40"></div>
		<div class="clear"></div>
	</div>
</section>
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('terlaris = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>
<section class="default_sc blocks_home2 back-orange">
	<div class="prelatife container">
		
		<?php if ($dataProduct): ?>
		<div class="block_product_data_wrap">
			<div class="top">
				<h6>Produk Terpopular</h6>
			</div>
			<div id="owl-demo" class="lists_product_data row">
				<?php foreach ($dataProduct as $key => $value): ?>
				<div class="col-md-12">
					<div class="items">
						<div class="picture prelatife">
							<?php if ($value->rekomendasi == 1): ?>
								<div class="boxs_inf_head_n1 back2"></div>
							<?php elseif ($value->onsale == 1): ?>
								<div class="boxs_inf_head_n1"></div>
							<?php else: ?>

							<?php endif ?>
							<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
							</a>
						</div>
						<div class="info description">
						<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<p><?php echo $value->description->name ?></p>
							<div class="block_price">
								<?php if ($value->harga_coret > $value->harga): ?>
								<span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
								<div class="clear"></div>
								<span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
								<div class="clear"></div>
								<?php else: ?>
								<span class="price_trough">&nbsp;</span>
								<div class="clear"></div>
								<span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">&nbsp;</span>
								<div class="clear"></div>
								<?php endif ?>
							</div>
						</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
			<div class="clear"></div>
		</div>
		<?php endif; ?>

		<?php
		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category', 'brand');
		$criteria->order = 'date DESC';
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('terbaru = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		// $pageSize = 12;
		$criteria->group = 't.id';
		$dataProduct = PrdProduct::model()->findAll($criteria);
		?>
		<?php if ($dataProduct): ?>
		<div class="block_product_data_wrap">
			<div class="top">
				<h6>Produk Koleksi Terbaru</h6>
			</div>
			<div id="owl-demo2" class="lists_product_data row">
				<?php foreach ($dataProduct as $key => $value): ?>
				<div class="col-md-12">
					<div class="items">
						<div class="picture prelatife">
							<?php if ($value->rekomendasi == 1): ?>
								<div class="boxs_inf_head_n1 back2"></div>
							<?php elseif ($value->onsale == 1): ?>
								<div class="boxs_inf_head_n1"></div>
							<?php else: ?>

							<?php endif ?>
							<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
							</a>
						</div>
						<div class="info description">
						<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<p><?php echo $value->description->name ?></p>
							<div class="block_price">
								<?php if ($value->harga_coret > $value->harga): ?>
								<span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
								<div class="clear"></div>
								<span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
								<div class="clear"></div>
								<?php else: ?>
								<span class="price_trough">&nbsp;</span>
								<div class="clear"></div>
								<span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">&nbsp;</span>
								<div class="clear"></div>
								<?php endif ?>
							</div>
						</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
			<div class="clear"></div>
		</div>
		<?php endif ?>


		<?php
		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category', 'brand');
		$criteria->order = 'date DESC';
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('turun_harga = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		// $pageSize = 12;
		$criteria->group = 't.id';
		$dataProduct = PrdProduct::model()->findAll($criteria);
		?>
		<?php if ($dataProduct): ?>
		<div class="block_product_data_wrap">
			<div class="top">
				<h6>Produk Turun Harga</h6>
			</div>
			<div id="owl-demo2" class="lists_product_data row">
				<?php foreach ($dataProduct as $key => $value): ?>
				<div class="col-md-12">
					<div class="items">
						<div class="picture prelatife">
							<?php if ($value->rekomendasi == 1): ?>
								<div class="boxs_inf_head_n1 back2"></div>
							<?php elseif ($value->onsale == 1): ?>
								<div class="boxs_inf_head_n1"></div>
							<?php else: ?>

							<?php endif ?>
							<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
							</a>
						</div>
						<div class="info description">
						<a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
							<p><?php echo $value->description->name ?></p>
							<div class="block_price">
								<?php if ($value->harga_coret > $value->harga): ?>
								<span class="price_trough">Harga Normal <i><?php echo Cart::money($value->harga_coret) ?></i></span>
								<div class="clear"></div>
								<span class="price">Kini <b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">Hemat RP <?php echo Cart::money($value->harga_coret - $value->harga) ?></span>
								<div class="clear"></div>
								<?php else: ?>
								<span class="price_trough">&nbsp;</span>
								<div class="clear"></div>
								<span class="price"><b><?php echo Cart::money($value->harga) ?></b></span>
								<div class="clear"></div>
								<span class="bottom_priceblack">&nbsp;</span>
								<div class="clear"></div>
								<?php endif ?>
							</div>
						</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
			<div class="clear"></div>
		</div>
		<?php endif ?>

		
		<div class="clear"></div>
	</div>
</section>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js" type="text/javascript"></script>

<script type="text/javascript">
// $.noConflict();
$(document).ready(function() {
  var owl = $("#owl-demo, #owl-demo2");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [768, 2], //2 items between 600 and 0
      itemsMobile : [600, 1], // itemsMobile disabled - inherit from itemsTablet option
  	  pagination: true,
  	  paginationNumbers: true,
  	  center:true
  });

  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })

});
</script>
<style type="text/css">
  .owl-carousel .owl-wrapper-outer{
    width: 99.6%;
  }
  section.default_sc.blocks_home2{
  	min-height: 60px; height: auto;
  }
</style>