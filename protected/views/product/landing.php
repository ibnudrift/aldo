<div class="clear"></div>
<div class="subpage defaults_static">

  <div class="top_title_page sub_toppage_title">
    <div class="prelatife container">
      <h2 class="title_pg">KATEGORI PILIHAN</h2>
    </div>
  </div>

  <div class="middle inside_content back-white">
    <div class="prelatife container">
			<!-- start Block -->
			<div class="content_category block_data_thum_products">
				<ul class="list-inline m-0">
					<?php foreach ($dataCategory as $key => $value): ?>
					<li class="list-inline-item">
						<div class="items">
							<div class="pic">
								<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(123,123, '/images/category/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->description->name; ?>" class="img img-fluid d-block mx-auto"></a>
							</div>
							<div class="info py-2 text-center">
								<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><span><?php echo $value->description->name ?></span></a>
							</div>
						</div>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<!-- end Block -->

    	<?php /*
    	<div class="lists_subCategory_dataLanding">
    		<div class="row">
    			<?php foreach ($subCategory as $key => $value): ?>
    			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
    				<div class="items text-center">
    					<div class="pict"><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(319,365, '/images/category/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a></div>
    					<div class="titles">
    						<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
    					</div>
    					<div class="clear"></div>
    				</div>
    			</div>
    			<?php endforeach ?>
    		</div>
    	</div>
    	<div class="clear height-20"></div>

    	<!-- End landing product -->
    	<div class="clear"></div>
    </div>
    	*/ ?>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>

<?php echo $this->renderPartial('//layouts/_block_orange', array()); ?>