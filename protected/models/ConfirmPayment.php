<?php

/**
 * This is the model class for table "tb_confirm_payment".
 *
 * The followings are the available columns in table 'tb_confirm_payment':
 * @property string $id
 * @property integer $order_id
 * @property integer $invoice_no
 * @property string $invoice_prefix
 * @property string $user_bank_name
 * @property string $user_bank_rek
 * @property string $user_bank_no
 * @property string $date_input
 * @property integer $aktif
 * @property integer $is_read
 */
class ConfirmPayment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConfirmPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_confirm_payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invoice, user_bank_name', 'required'),
			array('order_id, invoice, aktif, is_read', 'numerical', 'integerOnly'=>true),
			array('user_bank_name, user_bank_rek, user_bank_no', 'length', 'max'=>225),
			// The following rule is used by search().

			array('order_id, user_bank_rek, user_bank_no, date_input, aktif, total_transfer, tgl_transfer', 'safe'),

			// Please remove those attributes that should not be searched.
			array('id, order_id, invoice, user_bank_name, user_bank_rek, user_bank_no, date_input, aktif, is_read', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Order',
			// 'invoice_no' => 'Invoice No',
			// 'invoice_prefix' => 'Invoice Prefix',
			'user_bank_name' => 'Nama Bank Anda',
			'user_bank_rek' => 'Rekening Bank Anda',
			// 'user_bank_no' => 'No. ',
			'date_input' => 'Date Input',
			'aktif' => 'Aktif',
			'is_read' => 'Is Read',
			'tgl_transfer' => 'Tanggal Transfer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id, true);
		$criteria->compare('order_id',$this->order_id);
		// $criteria->compare('invoice_no',$this->invoice_no);
		// $criteria->compare('invoice_prefix',$this->invoice_prefix,true);
		$criteria->compare('user_bank_name',$this->user_bank_name,true);
		$criteria->compare('user_bank_rek',$this->user_bank_rek,true);
		$criteria->compare('user_bank_no',$this->user_bank_no,true);
		$criteria->compare('date_input',$this->date_input,true);
		$criteria->compare('aktif',$this->aktif);
		$criteria->compare('is_read',$this->is_read);
		$criteria->compare('tgl_transfer',$this->tgl_transfer);
		
		$criteria->order = 't.date_input DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}