<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_id')); ?>:</b>
	<?php echo CHtml::encode($data->order_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invoice_no')); ?>:</b>
	<?php echo CHtml::encode($data->invoice_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invoice_prefix')); ?>:</b>
	<?php echo CHtml::encode($data->invoice_prefix); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_bank_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_bank_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_bank_rek')); ?>:</b>
	<?php echo CHtml::encode($data->user_bank_rek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_bank_no')); ?>:</b>
	<?php echo CHtml::encode($data->user_bank_no); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date_input')); ?>:</b>
	<?php echo CHtml::encode($data->date_input); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aktif')); ?>:</b>
	<?php echo CHtml::encode($data->aktif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_read')); ?>:</b>
	<?php echo CHtml::encode($data->is_read); ?>
	<br />

	*/ ?>

</div>