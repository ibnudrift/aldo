<?php
$this->breadcrumbs=array(
	'Confirm Payment',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Confirm Payment',
	'subtitle'=>'Data Confirm Payment',
);

$this->menu=array(
	// array('label'=>'Add Confirm Payment', 'icon'=>'th-list','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Confirm Payment</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'confirm-payment-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'rowCssClassExpression'=>'($data->is_read == 0) ? "row-bold" : ""',
	'columns'=>array(
		// 'id',
		// 'order_id',
		
		array(
			'header'=>'Invoice',
			'type'=>'raw',
			'value'=>'CHtml::link($data->invoice, array("/admin/order/detail", "id"=>$data->order_id))',
		),

		'user_bank_name',
		'user_bank_rek',
		'total_transfer',
		
		array(
			'header'=>'DATE',
			'type'=>'raw',
			'value'=>'date("d-M-Y", strtotime($data->date_input))',
		),
		// 'date_input',
		/*
		'aktif',
		'is_read',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
