<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'confirm-payment-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data ConfirmPayment</h4>
<div class="widgetcontent">

	<?php // echo $form->textFieldRow($model,'order_id',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'invoice_no',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'invoice',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php // echo $form->textFieldRow($model,'invoice_prefix',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'user_bank_name',array('class'=>'span5','maxlength'=>225, 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'user_bank_rek',array('class'=>'span5','maxlength'=>225, 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'total_transfer',array('class'=>'span5','maxlength'=>225, 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'tgl_transfer',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php // echo $form->textFieldRow($model,'aktif',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'is_read',array('class'=>'span5')); ?>

	<?php 
	// $this->widget('bootstrap.widgets.TbButton', array(
	// 	'buttonType'=>'submit',
	// 	'type'=>'primary',
	// 	'label'=>$model->isNewRecord ? 'Add' : 'Save',
	// )); 
	?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'url'=>CHtml::normalizeUrl(array('index')),
		'label'=>'Batal',
	)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
