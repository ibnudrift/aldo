<?php
$this->breadcrumbs=array(
	'Confirm Payments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ConfirmPayment', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add ConfirmPayment', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit ConfirmPayment', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete ConfirmPayment', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View ConfirmPayment #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'order_id',
		'invoice_no',
		'invoice_prefix',
		'user_bank_name',
		'user_bank_rek',
		'user_bank_no',
		'date_input',
		'aktif',
		'is_read',
	),
)); ?>
