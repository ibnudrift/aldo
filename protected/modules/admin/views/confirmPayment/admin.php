<?php
$this->breadcrumbs=array(
	'Confirm Payments'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ConfirmPayment','url'=>array('index')),
	array('label'=>'Add ConfirmPayment','url'=>array('create')),
);
?>

<h1>Manage Confirm Payments</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'confirm-payment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'order_id',
		'invoice_no',
		'invoice_prefix',
		'user_bank_name',
		'user_bank_rek',
		/*
		'user_bank_no',
		'date_input',
		'aktif',
		'is_read',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
